import { Printer } from "../printer";
import { Node } from "../parser";
declare const _default: {
    [type: string]: Printer<Node, any, string | HTMLElement | Comment>;
};
export default _default;
