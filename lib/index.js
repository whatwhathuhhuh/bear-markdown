"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var parser_1 = require("./parser");
exports.newParser = parser_1.newParser;
var printer_1 = require("./printer");
exports.newPrinter = printer_1.newPrinter;
var printers_1 = require("./printers");
var rules_1 = require("./rules");
exports.defaultRules = rules_1.defaultRules;
exports.defaultParser = parser_1.newParser(rules_1.defaultRules);
exports.defaultHTMLPrinter = printer_1.newPrinter(printers_1.html);
//# sourceMappingURL=index.js.map